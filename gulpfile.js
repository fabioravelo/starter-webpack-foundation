// Variables iconfont
var fontName = 'ic';

var gulp = require('gulp');
var $    = require('gulp-load-plugins')(),
    sourcemaps = require('gulp-sourcemaps'),
    iconfont = require('gulp-iconfont'),
    iconfontCss = require('gulp-iconfont-css');
var exec = require('child_process').exec;

// FOUNDATION
 var sassPaths = [
   'node_modules/normalize.scss',
   'node_modules/foundation-sites/scss',
   'node_modules/motion-ui/src'
 ];

gulp.task('sass', function() {
  return gulp.src('src/scss/*.scss')
  .pipe(sourcemaps.init())
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('assets/css'));
});

// Font iconfont
var runTimestamp = Math.round(Date.now()/1000);

gulp.task('iconfont', function(){
  gulp.src(['assets/images/icons/*.svg'])
    .pipe(iconfontCss({
      fontName: fontName, // nom de la fonte, doit être identique au nom du plugin iconfont
      // path: 'scss/modules/_icons-fonts.scss',
      targetPath: '../../../src/scss/modules/_icons-fonts.scss', // emplacement de la css finale
      fontPath: '../../assets/fonts/icons/', // emplacement des fontes finales
      cssClass: fontName
    }))
    .pipe(iconfont({
      fontName: fontName, // required - identique au nom de iconfontCss
      prependUnicode: true, // recommended option
      formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'], // default, 'woff2' and 'svg' are available
      timestamp: runTimestamp, // recommended to get consistent builds when watching files
    }))
    .pipe(gulp.dest('assets/fonts/icons/'));
});

gulp.task('webpack', function (cb) {
  // Exécution cmd 'webpack'
  exec('webpack', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
})

gulp.task('default', ['iconfont', 'sass', 'webpack'], function() {
  gulp.watch(['src/scss/**/*.scss'], ['sass']);
  gulp.watch(['assets/images/icons/*.svg', 'assets/images/icons/*.svg'], ['iconfont']);
  gulp.watch(['src/js/*.js'], ['webpack']);
});
